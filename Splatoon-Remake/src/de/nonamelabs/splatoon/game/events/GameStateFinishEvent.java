package de.nonamelabs.splatoon.game.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import de.nonamelabs.splatoon.game.GameState;

public class GameStateFinishEvent extends Event {
	
	private static final HandlerList handlers = new HandlerList();

	private GameState gamestate;
	public GameStateFinishEvent(GameState gamestate) {
		this.gamestate = gamestate;
	}
	
	public GameState getGameState() {
		return gamestate;
	}
	
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}
}
