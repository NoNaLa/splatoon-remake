package de.nonamelabs.splatoon.game.weapon;

public enum WeaponType {
	PRIMARY, SECONDARY, SPECIAL
}
