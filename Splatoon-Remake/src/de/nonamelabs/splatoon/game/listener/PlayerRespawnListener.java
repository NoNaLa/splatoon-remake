package de.nonamelabs.splatoon.game.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;

import de.nonamelabs.splatoon.game.Game;
import de.nonamelabs.splatoon.game.GameState;
import de.nonamelabs.splatoon.game.PlayerHandler;
import de.nonamelabs.splatoon.game.gamestate.GameStateBase.PrepareType;

public class PlayerRespawnListener implements Listener{

	Game game;
	
	public PlayerRespawnListener(Game game){
		this.game = game;
	}
	
	@EventHandler
	public void onPlayerRespawn(PlayerRespawnEvent event){
		Player p = event.getPlayer();
		if (game.getPlayerHandler().isPlayer(p)) {
			game.getState().preparePlayer(p, PrepareType.DEATH);
		} else {
			game.getState().prepareSpectator(p, PrepareType.DEATH);
		}
		PlayerHandler pl = game.getPlayerHandler();
		if(game.getGameState() != GameState.INGAME) event.setRespawnLocation(game.getLobby());
		else event.setRespawnLocation(pl.getTeamSpawn(pl.getTeam(p)));
	}
}
