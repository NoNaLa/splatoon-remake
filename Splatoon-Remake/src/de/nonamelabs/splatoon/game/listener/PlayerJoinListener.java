package de.nonamelabs.splatoon.game.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import de.nonamelabs.splatoon.game.Game;
import de.nonamelabs.splatoon.game.GameState;
import de.nonamelabs.splatoon.util.ChatUtil;
import de.nonamelabs.splatoon.util.PlayerUtil;

public class PlayerJoinListener implements Listener{

	private Game game;
	public PlayerJoinListener(Game game) {
		this.game = game;
	}
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		if (game.getGameState() == GameState.UNSTARTET) return;
		
		event.setJoinMessage("");
		
		Player p = event.getPlayer();
		
		game.getPlayerHandler().addPlayer(p);
		PlayerUtil.setTabTitle(p, ChatUtil.PLUGIN_COLOR + "SPLATOON", ChatUtil.HIGHLIGHT_COLOR + "by PhoenixofForce and Nottrex");
	}	
}