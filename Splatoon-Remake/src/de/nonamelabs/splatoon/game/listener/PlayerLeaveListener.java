package de.nonamelabs.splatoon.game.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import de.nonamelabs.splatoon.game.Game;
import de.nonamelabs.splatoon.game.GameState;

public class PlayerLeaveListener implements Listener{

	private Game game;
	public PlayerLeaveListener(Game game) {
		this.game = game;
	}
	
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		if (game.getGameState() == GameState.UNSTARTET) return;
		
		event.setQuitMessage("");
		
		Player p = event.getPlayer();
		
		game.getPlayerHandler().removePlayer(p);
	}	
}