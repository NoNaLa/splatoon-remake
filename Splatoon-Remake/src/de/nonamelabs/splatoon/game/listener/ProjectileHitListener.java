package de.nonamelabs.splatoon.game.listener;

import org.bukkit.Location;
import org.bukkit.entity.Egg;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileHitEvent;

import de.nonamelabs.splatoon.game.Game;
import de.nonamelabs.splatoon.game.GameState;
import de.nonamelabs.splatoon.game.TeamColor;
import de.nonamelabs.splatoon.util.Util;


public class ProjectileHitListener implements Listener {
	
	private Game game;

	public ProjectileHitListener(Game game) {
		this.game = game;
	}
	
	@EventHandler
	public void onProjectileHit(ProjectileHitEvent event) {
		if (game.getGameState() != GameState.INGAME) return;
		
		if (event.getEntity() instanceof Projectile) {
			Projectile projectile = (Projectile) event.getEntity();
			String name = projectile.getCustomName();
			
			if (projectile.getShooter() instanceof Player && name.split(" ").length == 3) {
				
				Location l = projectile.getLocation().clone();
				Player p = (Player) projectile.getShooter();
				TeamColor c = game.getPlayerHandler().getTeam(p);
				String[] parts = name.split(" ");

				int damage = Integer.valueOf(parts[1]);
				int area = Integer.valueOf(parts[2]);
				
				if (projectile instanceof Snowball || projectile instanceof Egg) {
					for (int x = l.getBlockX() - area; x <= l.getBlockX() + area; x++) {
						for (int y = l.getBlockY() - area; y <= l.getBlockY() + area; y++) {
							for (int z = l.getBlockZ() - area; z <= l.getBlockZ() + area; z++) {
								if ((Math.abs(l.getBlockX()-x) + Math.abs(l.getBlockY()-y) + Math.abs(l.getBlockZ()-z)) <= area) {
									if (Util.setBlockTeam(l.getWorld().getBlockAt(x, y, z), c)) {
										game.getPlayerHandler().getGamePlayer(p).addScore(1);
									}
								}
							}
						}
					}
				}
				
				for (Entity e: projectile.getNearbyEntities(area, area, area)) {
					if (e instanceof Player) {
						Player p2 = (Player) e;
						if (game.getPlayerHandler().isPlayer(p2) && game.getPlayerHandler().getTeam(p2) != game.getPlayerHandler().getTeam(p)) {
							p2.damage(damage, p);
						}
					}
				}
			}			
		}
	}
}
