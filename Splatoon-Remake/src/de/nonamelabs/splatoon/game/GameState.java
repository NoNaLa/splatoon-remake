package de.nonamelabs.splatoon.game;

public enum GameState {
	UNSTARTET, LOBBY, COUNTDOWN, INGAME, END
}
